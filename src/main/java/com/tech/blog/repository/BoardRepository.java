package com.tech.blog.repository;

import com.tech.blog.domain.BoardEntity;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class BoardRepository {

    @PersistenceContext
    private EntityManager em;

    public BoardEntity findOne(Long id) {
        return em.find(BoardEntity.class, id);
    }
    
    public List<BoardEntity> findAll() {
        return em.createQuery("select m from BoardEntity m", BoardEntity.class)
                .getResultList();
    }
    
    public List<BoardEntity> findByName(String name) {
        return em.createQuery("select m from BoardEntity m where m.name = :name",
                        BoardEntity.class)
                .setParameter("name", name)
                .getResultList();
    }
    
}
