package com.tech.blog.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter @Setter
@Table(name = "TB_CATEGORY")
public class CategoryEntity extends BaseEntity {

    @Id
    @Column(name = "ctgry_sno")
    private Long ctgrySno;

    @Column(name = "ctgry_nm")
    private String ctgryNm;

    @Column(name = "ctgry_nm")
    private Long upperCtgrySno;

}
