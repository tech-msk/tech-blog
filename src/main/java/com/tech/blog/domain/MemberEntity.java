package com.tech.blog.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter @Setter
@Table(name = "TB_MEMBER")
public class MemberEntity extends BaseEntity {

    @Id
    @Column(name = "mber_sno")
    private Long mberSno;

    @Column(name = "mber_id")
    private String mberId;

    @Column(name = "mber_nm")
    private String mberNm;

    @Column(name = "mber_pswd")
    private String mberPswd;

}
