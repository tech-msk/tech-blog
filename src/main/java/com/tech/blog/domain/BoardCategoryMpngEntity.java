package com.tech.blog.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Getter @Setter
@Table(name = "TB_BOARD_CATEGORY_MPNG")
public class BoardCategoryMpngEntity extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "bbs_sno")
    private BoardEntity boardEntity;

    @ManyToOne
    @JoinColumn(name = "ctgry_sno")
    private CategoryEntity categoryEntity;

}
