package com.tech.blog.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter @Setter
@Table(name = "TB_BOARD")
public class BoardEntity extends BaseEntity {

    @Id
    @Column(name = "bbs_sno")
    private Long bbsSno;

    @Column(name = "title")
    private String title;

    @Column(name = "cn")
    private String cn;

    @OneToMany(mappedBy = "answer")
    private List<AnswerEntity> answers = new ArrayList<>();
}
