package com.tech.blog.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter @Setter
@Table(name = "TB_ANSWER")
public class AnswerEntity extends BaseEntity {

    @Id
    @Column(name = "answer_sno")
    private Long answerSno;

    @Column(name = "upper_answer_sno")
    private Long upperAnswerSno;

    @Column(name = "cn")
    private String cn;

    @ManyToOne
    @JoinColumn(name = "bbs_sno")
    private BoardEntity boardEntity;

    @OneToOne(mappedBy = "member")
    @JoinColumn(name = "regpe_sno")
    private MemberEntity memberEntity;

}
