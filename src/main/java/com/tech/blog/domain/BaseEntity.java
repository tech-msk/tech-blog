package com.tech.blog.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.time.LocalDateTime;

@Entity
@Getter @Setter
public class BaseEntity {

    @Column(name = "regpe_sno")
    private Long regpeSno;

    @Column(name = "regpe_ip_adres")
    private String regpeIipAdres;

    @Column(name = "reg_dtm")
    private LocalDateTime regDtm;

    @Column(name = "mdfpe_sno")
    private Long mdfpeSno;

    @Column(name = "mdfpe_ip_adres")
    private String mdfpeIipAdres;

    @Column(name = "mdf_dtm")
    private LocalDateTime mdfDtm;

    @Column(name = "use_yn")
    private String useYn;

    @Column(name = "del_yn")
    private String delYn;

}
